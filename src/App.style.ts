import { StyleSheet } from "react-native";
import Constants from "expo-constants";

export default StyleSheet.create({
  loadingView: {
    flex: 1,
    padding: 100
  },
  loading: {
    textAlign: "center",
    marginBottom: 10
  },
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight
  },
  h1: {
    fontSize: 25,
    backgroundColor: "#FF5700",
    padding: 10,
    lineHeight: 30,
    color: "black",
    fontWeight: "bold"
  }
});
