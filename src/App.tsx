import React from 'react';
import { FlatList, ActivityIndicator, Text, View, SafeAreaView } from 'react-native';
import Api from './modules/api';
import { exhibitionItem } from './shared/interfaces/interfaces';
import ExhibitionItem from './shared/components/ExhibitionItem';
import styles from './App.style';

export default class CooperHewittApp extends React.Component {

  state: {
    isLoading: boolean;
    objects: any | null;
  }

  constructor(props: Readonly<{}>) {
    super(props);
    this.state = { isLoading: true, objects: null };
  }

  componentDidMount() {
    const api = new Api();
    api.getExhibitionById('421207063').then((responseJson: any) => {
      this.setState({
        isLoading: false,
        objects: responseJson,
      });
    })
  }

  render() {

    if (this.state.isLoading) {
      return (
        <View style={styles.loadingView}>
          <Text style={styles.loading}>Loading...</Text>
          <ActivityIndicator size="large" color="#FF5700" />
        </View>
      )
    }

    return (
      <SafeAreaView style={styles.container}>
        <Text style={styles.h1}>PASSION FOR THE EXOTIC: JAPONISM</Text>
        <FlatList
          data={this.state.objects}
          renderItem={({ item }) => <ExhibitionItem itemData={item} />}
          keyExtractor={(item: exhibitionItem) => item.id.toString()}
        />
      </SafeAreaView>
    );
  }
}
