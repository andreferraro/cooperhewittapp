import React from 'react';
import { Text, View, Image } from 'react-native';
import { Props } from '../interfaces/interfaces';
import ModalDetails from '../ModalDetails';
import commonStyles from './common.style';

class ExhibitionItem extends React.Component<Props> {
  render() {
    const { itemData } = this.props;
    return (
      <View style={commonStyles.item}>
        <Text style={commonStyles.title}>{itemData.title}</Text>
        <View style={commonStyles.imageView}>
          <Image style={commonStyles.image} source={{ uri: itemData.images[0].n.url }} />
        </View>
        <Text style={commonStyles.date}>{itemData.date}</Text>
        <Text style={commonStyles.description}>{itemData.description}</Text>
        <ModalDetails itemData={itemData} />
      </View>
    )
  }
}

export default ExhibitionItem;
