import { StyleSheet } from "react-native";

export default StyleSheet.create({
  item: {
    backgroundColor: "#f5f5f5",
    borderColor: "#dbdbdb",
    borderWidth: 1,
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 12
  },
  title: {
    fontSize: 20,
    marginBottom: 10,
    textAlign: "center"
  },
  date: {
    fontSize: 16,
    fontWeight: "bold",
    marginBottom: 10
  },
  description: {
    fontSize: 15,
    lineHeight: 22,
    fontWeight: "normal"
  },
  imageView: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1
  },
  image: {
    width: 150,
    height: 150,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "gray"
  }
});
