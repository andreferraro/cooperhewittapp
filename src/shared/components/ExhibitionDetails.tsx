import React from 'react';
import { Text, View, Image } from 'react-native';
import { Props, exhibitionItem, images } from '../interfaces/interfaces';
import commonStyles from './common.style';
import styles from './ExhibitionDetails.style';

class ExhibitionDetails extends React.Component<Props> {

  renderImages = (itemData: exhibitionItem) => {
    const { images } = itemData;
    let contentImages = [];

    images.map((image: images, index: number) => {
      if (index > 0) {
        contentImages.push(<Image style={styles.imageGallery} source={{ uri: image.n.url }} key={index.toString()} />);
      }
    });
    
    return contentImages;
  }

  render() {
    const { itemData } = this.props;
    return (
      <View style={commonStyles.item}>
        <Text style={commonStyles.title}>{itemData.title}</Text>
        <View style={commonStyles.imageView}>
          <Image style={commonStyles.image} source={{ uri: itemData.images[0].n.url }} />
        </View>
        <View style={styles.galleryView}>
          {this.renderImages(itemData)}
        </View>
        <Text style={styles.strong}>Credits: <Text style={styles.notStrong}>{itemData.creditline}</Text></Text>
        <Text style={styles.strong}>Date: <Text style={styles.notStrong}>{itemData.date}</Text></Text>
        <Text style={styles.strong}>Provenance: <Text style={styles.notStrong}>{itemData.provenance}</Text></Text>
        <Text style={styles.strong}>Dimensions: <Text style={styles.notStrong}>{itemData.dimensions}</Text></Text>
        <Text style={styles.strong}>Description:</Text>
        <Text style={commonStyles.description}>{itemData.description}</Text>
        <Text style={styles.strong}>Gallery Description:</Text>
        <Text style={commonStyles.description}>{itemData.gallery_text}</Text>
      </View>
    )
  }
}

export default ExhibitionDetails;
