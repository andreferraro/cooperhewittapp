import { StyleSheet } from "react-native";

export default StyleSheet.create({
  strong: {
    fontWeight: "bold"
  },
  notStrong: {
    fontWeight: "normal"
  },
  galleryView: {
    justifyContent: "center",
    flexDirection: "row",
    flexWrap: "wrap",
    flexShrink: 3,
    marginTop: 20,
    marginBottom: 20
  },
  imageGallery: {
    width: 100,
    height: 100,
    padding: 5,
    borderColor: "#ede6e6",
    borderWidth: 1,
    margin: 5
  }
});
