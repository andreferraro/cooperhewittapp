import React from 'react';
import { Modal, Button, StyleSheet, SafeAreaView, ScrollView } from 'react-native';
import Constants from 'expo-constants';
import { Props } from './interfaces/interfaces';
import ExhibitionDetails from './components/ExhibitionDetails';

class ModalDetails extends React.Component<Props> {

  state = {
    modalVisible: false,
  };

  setModalVisible(visible: boolean) {
    this.setState({ modalVisible: visible });
  }

  render() {
    const { itemData } = this.props;

    return (
      <SafeAreaView style={styles.container}>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setModalVisible(false);
          }}>
          <ScrollView>
            <ExhibitionDetails itemData={itemData} />
          </ScrollView>

          <Button
            color='#83654f'
            title="Close"
            onPress={() => {
              this.setModalVisible(false);
            }}
          />
        </Modal>

        <Button
          color='#83654f'
          title="Show Details"
          onPress={() => {
            this.setModalVisible(true);
          }}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
  },
});

export default ModalDetails;
