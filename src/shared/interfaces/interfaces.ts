export interface Props {
  itemData: exhibitionItem | null;
}

export interface exhibitionItem {
  id: number;
  title: string | null;
  images: images[] | null;
  date: string | null;
  description: string | null;
  gallery_text: string | null;
  provenance: string | null;
  dimensions: string | null;
  creditline: string | null;
}

export interface images {
  n: exhibitionItemImage;
}

interface exhibitionItemImage {
  url: string;
}

export interface exhibition {
  title: string;
  id: number;
}
