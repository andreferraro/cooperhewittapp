import config from "../config.json";

export default class Api {
  urlBase: string;
  token: string;

  constructor() {
    this.urlBase = config.urlApi;
    this.token = config.token;
  }

  getExhibitionById = async (ExhibitionId: string) => {
    try {
      const response = await fetch(
        `${this.urlBase}?method=cooperhewitt.exhibitions.getObjects&access_token=${this.token}&exhibition_id=${ExhibitionId}&page=1&per_page=100`
      );
      const responseJson = await response.json();
      return responseJson.objects;
    } catch (error) {
      console.error(error);
    }
  };
}
